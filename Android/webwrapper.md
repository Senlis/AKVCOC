The code below highlights how we can override default web view behavior to create custom behavior.  In this instance, we can override the default behavior of onPageStarted and onPageFinished to show the loading gif when a page is loading and hide it when the page is finished.

``` java
public class MainActivity extends AppCompatActivity
{

    private WebView my_web_view;      //create a webview variable
    private pl.droidsonroids.gif.GifImageView vcoc_loading;  //create a GIF view variable
    
    private String home_URL;          //holds the URL string for the homepage.  Other variables snipped
    //snipped

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //snipped
        
        //Here, we find the webview and GIF view we created on the layout screen by using thier ID
        my_web_view = (WebView)findViewById(R.id.web_view); 
        vcoc_loading = (pl.droidsonroids.gif.GifImageView)findViewById(R.id.vcoc_loading);

        home_URL = "https://akvcoc.com/app-home";  //The homepage
        //snipped

        my_web_view.loadUrl(home_URL);                    //set the webview to load to the home page

        //snipped
        
        my_web_view.setWebViewClient(new CustomWebViewClient());     //set our webview behavior to our custom created client
    }

    //This is where we define our custom client
    private class CustomWebViewClient extends WebViewClient { //our custom client extends WebViewClient, the default behavior
        
        @Override //override means we are writing over a pre-built function
        public void onPageStarted(WebView webview, String url, Bitmap favicon) {
            vcoc_loading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            vcoc_loading.setVisibility(View.INVISIBLE);

            super.onPageFinished(view, url); //by calling "super", we have the class run it's default logic
        }
}
```
