/*
AKVCOC: web wrapper for church's website and youtube player.
App developed by Richard Romick for use by the Alaska Valley Church of Christ.
Programming help and advice provided by Jacob Harris.
Backend development and UI direction provided by Brady Kuenning.
		Copyright (C) 2018  Richard Romick

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package app_vcoc.akvcoc;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;


public class YoutubePlayer extends YouTubeBaseActivity {
	//==================================== Class variable declarations =========================================
	//view objects that we will use to interact with the UI
    private YouTubePlayerView my_youtube_view;	//main youtube player view
	private ImageView pause_play_button;		//redundant pause/play button
	private TextView not_streaming_view; 		//message for live stream is down

    private String API_key;						//Protected youtube API key. Stored at /main/src/main/assets/youtubeAPIkey.txt
	private String URL;							//Stored URL from transition to this view.  Used if back button is pressed
	private String video_ID;					//Live Video ID retrieved from JSON request (see JSON section below)
	final String VCOC_channel_ID = "UCgETYmGUhEmeYYYxVgXKmAA"; //Channel ID for AKVCOC
	final String Test_channel_ID = "UCAOtE1V7Ots4DjM8JLlrYgg"; //Channel that is always streaming live for testing
		//test Youtube URL (Peppa Pig channel): https://www.youtube.com/channel/UCAOtE1V7Ots4DjM8JLlrYgg

    private YouTubePlayer player;				//the youtube player itself (not the UI object itself, but closely related to it)
	//---------------------------------- end Class variable declarations ---------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
		/*
        Youtube Activity constructor
        Initializes youtube player and retrieves video ID to load
        @param Bundle savedInstanceState: saved state from screen rotation
         */

		//==================================== Admin stuff =========================================
    	//activity setup
		video_ID = null;  //we will get the Youtube video ID from the JSON request below
		super.onCreate(savedInstanceState);  //Call the parent view's onCreate method (I have no idea what it does)
		setContentView(R.layout.activity_youtube_player);  //this ties the activity_youtube_player.xml file to this class

    	//get handles on UI elements
		pause_play_button = findViewById(R.id.pause_play_button);
		BottomNavigationView navigation = findViewById(R.id.navigation);
		my_youtube_view = findViewById(R.id.youtube_player);
		not_streaming_view = findViewById(R.id.not_streaming_text);

		//Retrieve URL that was passed to this view in case back button is pressed
		Bundle b = getIntent().getExtras();
		URL = "";

		if(b != null)
		{
			URL = b.getString("URL");
		}
		//---------------------------------- end admin stuff ---------------------------------------

		//==================================== Read API Youtube key from file =========================================
	    try
	    {
		    InputStream is = getAssets().open("youtubeAPIkey.txt"); //Stored at /main/src/main/assets/youtubeAPIkey.txt
		    int size = is.available();
		    byte[] buffer = new byte[size];
		    is.read(buffer);
		    is.close();
		    API_key = new String(buffer);
	    }
	    catch (IOException e)
	    {
		    e.printStackTrace();
	    }
		//---------------------------------- end read Youtube API key from file ---------------------------------------


		//==================================== Navigation bar setup =========================================
		navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
		{
			/*
				Custom navigation listener for bottom navigation bar that listens and reacts to button clicks
			 */
			@Override
			public boolean onNavigationItemSelected(@NonNull MenuItem item) 
			{
			    /*
			        Function is activated when a navigation button is clicked
			        @param - MenuItem item - the button that was clicked
			    */
				switch (item.getItemId()) //ItemId is set by the UI designer
				{
					case R.id.navigation_home: //home button
						change_to_main_activity(0); //this function handles switching to the main activity.
						                            //and signals to that activity to load the home page
						break;
					case R.id.navigation_events: //events button
						change_to_main_activity(1); //change to main activity and load events page
						break;
					case R.id.navigation_directory: //directory button
						change_to_main_activity(3); //change to main activity and load directory page
						break;
					case R.id.navigation_live: //live button
						break; //do nothing if live button is pressed
					default:
						return false;
				}
				return true;
			}
		});

		navigation.getMenu().getItem(2).setChecked(true); //for initial setup, highlight (darken) the live button
		//---------------------------------- end Navigation bar setup ---------------------------------------

		initialize_youtube_player(); //this function will initialize the youtube player and take the necessary steps to load
		                            //the live steam, assuming that stream is active
    }

	//==================================== Youtube player setup =========================================
    private void initialize_youtube_player()
	{
		/*
		function initialized youtube player and, on success, calls the JSON function that finds the video ID and starts the video
		 */
		my_youtube_view.initialize(API_key, new YouTubePlayer.OnInitializedListener()
		{
		    //the youtube initialization defined above will continue and complete independant of the code below
		    //in other words, the view will _probably_ finish loading before the onInitializationSuccess function
		    //executes and, subsequently, the JSON request.  A loading spinning wheel will be displayed while the
		    //Youtube UI object is waiting for the initialization to complete.
		    
			/*
            On Initialized listener acts upon youtube view initialization success or failure
             */
			@Override
			public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b)
			{
		    	/*
		    	Youtube view listener function
		    	Activates after initialization is successful
		    	Create youtube player, create playback listener, run JSON request and load video.
		    	@param Provider provider - not used
		    	@param YouTubePlayer youTubePlayer - turned into class variable.  Used to control playback
		    	@param boolean b - not used
		    	 */

		    	/*
				Create Youtube player
				This is a separate object than the Youtube view, but they work together
				Youtube player will control loading, pausing, playing, etc
				*/
				player = youTubePlayer;
				player.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener()
				{
					/*
			    	Youtube playback listener
			    	Overrides playback events such as play and pause
			    	 */
			    	 //todo: do I need to invode the super function on the below functions
					@Override
					public void onPaused()
					{
				    	/*
				    	Youtube playback function
				    	Updates pause/play button when user pauses Youtube view directly in UI element
				    	 */
						pause_play_button.setImageResource(R.mipmap.play);
					}

					@Override
					public void onPlaying()
					{
				    	/*
				    	Youtube playback function
				    	Updates pause/play button when user pauses Youtube view directly in UI element
				    	 */
						pause_play_button.setImageResource(R.mipmap.pause);
					}

					@Override
					public void onBuffering(boolean isBuffering)
					{
						//no changes needed
					}

					@Override
					public void onStopped()
					{
						//no changes needed
					}

					@Override
					public void onSeekTo(int newPositionMillis)
					{
						//no changes needed
					}
				});

				conduct_JSON_request(); //request video ID and load live steam if active

			}
			@Override
			public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult)
			{
				//todo: some error handling if the youtube player fails to initialize
			}
		});
	}
	//---------------------------------- end Youtube player setup ---------------------------------------
	
	//==================================== JSON request =========================================

	
	private void conduct_JSON_request()
	{
		/*
		function finds the video ID and starts the youtube video
		The JSON request is necessary to convert the AKVCOC channel ID into the video ID of the currently streaming video
	    The channel ID is constant, but the video ID changes each time a live video is started
	    
	    The JSON request is initiated by the initialization success function in Youtube player setup
	    to ensure that a video is not loaded before the Youtube player is finished initializing
		 */
		//todo: safety check for initialized youtube player
		JsonObjectRequest json_data;		//stores JSON data from JSON request

		//set JSON query URL
		final String JSON_query_URL = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId="
				+ VCOC_channel_ID + "&eventType=live&type=video&key=" + API_key;

		//test JSON query URL
//		final String JSON_query_URL = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId="
//				+ Test_channel_ID + "&eventType=live&type=video&key=" + API_key;

		final RequestQueue queue = Volley.newRequestQueue(this);  //Volley is used to conduct the JSON request (see end of function)

		json_data = new JsonObjectRequest(Request.Method.GET, JSON_query_URL, null,
				new Response.Listener<JSONObject>()
				{
					/*
                    JSON object listener
                    Listens for JSON response, parses results and then loads youtube video
                    @param JSON_query_URL - contains the URL of the JSON query is going to obtain data from
                     */
					@Override
					public void onResponse(JSONObject response)
					{
						    	/*
						    	JSON listener
						    	Runs when JSON response is obtained
						    	@param JSONObject response - contains JSON response data
						    	 */
							    try
							    {
							    	//begin parsing JSON data
								    JSONObject page_data = response.getJSONObject("pageInfo"); //pull out JSON results
								    Integer num_results = page_data.getInt("totalResults"); //total number of live steams
								    if (num_results > 0) //this is the case where we have a live video
								    {
									    //here, we have a live video, so we drill down and find the ID
									    JSONArray video_array = response.getJSONArray("items"); //narrow down to a specific video
									    JSONObject video_data = video_array.getJSONObject(0);
									    JSONObject video_ID_data = video_data.getJSONObject("id"); 
									    video_ID = video_ID_data.getString("videoId");

									    player.loadVideo(video_ID); //load the player with the correct video ID

									    //flag causes video to enter landscape if fullscreen button is pressed
									    player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);

									    //flag causes video to enter fullscreen if app is rotated to landscape
									    player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
								    }
								    else  //this is the case where we don't have a live video
								    {
								    	//if no video live, make the youtube view invisible and then display no live video message
									    my_youtube_view.setVisibility(View.INVISIBLE);
									    not_streaming_view.setVisibility(View.VISIBLE);
									    //todo: stop the youtube player from trying to initialize if there is no live video
								    }
							    }
							    catch (JSONException e)
							    {
								    //no action needed as youtube player isn't going to load if there is an error
							    }

					}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error)
					{
						//todo: better error handling?
					}
				});

			queue.add(json_data); //starts the JSON request process
	}

	//---------------------------------- end JSON request ---------------------------------------

    //==================================== Transitions between main and Youtube views =========================================
	private void change_to_main_activity(int button)
	{
		/*
		Function changes to web view activity using intents
		Used when a bottom navigation bar button is pressed
		@param int button - indicates which button was pressed
		 */
		Intent intent = new Intent(YoutubePlayer.this, MainActivity.class);
		Bundle b = new Bundle();
		b.putInt("button", button); //store the button number
		intent.putExtras(b); //store the button number
		startActivity(intent);
		this.overridePendingTransition(0, 0); //cancel activity change animation
		finish();
	}

	private void change_to_main_activity(String u)
	{
		/*
		Function changes to web view activity using intents
		Used when back button is pressed
		Also used to load the web page with previous sermons when recorded services is pressed
		@param String u - the URL the web view is going to load
		 */
		Intent intent = new Intent(YoutubePlayer.this, MainActivity.class);
		Bundle b = new Bundle();
		b.putInt("button", 2);  //this is a dummy button number to indicate one wasn't pressed
		b.putString("URL", u); //Your id
		intent.putExtras(b); //Put your id to your next Intent
		startActivity(intent);
		this.overridePendingTransition(0, 0); //canvel activity change animation
		finish();
	}
	//---------------------------------- end Transitions between views ---------------------------------------

    //==================================== UI events =========================================
    public void play_pause_on_click(View view)
    {
    	/*
    	Function is called when pause/play button is pressed
    	Plays/Pauses the youtube player
    	Then updates the graphic on the play/pause button
    	@param View view - the pause/play button
    	 */
        if (player.isPlaying()) //case that the player is currently playing
        {
            player.pause();
            pause_play_button.setImageResource(R.mipmap.play); //changes pause/play button image
        }
        else //case that the player is currently playing
		{
            player.play();
            pause_play_button.setImageResource(R.mipmap.pause); //changes pause/play button image
        }
    }

    public void onBackPressed()
    {
        /*
        Function activates when back button is pressed
        Change back to the main activity using the last URL before the Youtube Activity was loaded
         */
		change_to_main_activity(this.URL);
    }

	public void recorded_services_button_click(View view)
	{
		/*
		Event function for when recorded services button is pressed
		Change to Web View and load the playlist of previous services
		@param view: the view initiating the event
		 */
		this.change_to_main_activity("https://www.youtube.com/user/AKVCOC/videos");
	}

	//---------------------------------- end UI events ---------------------------------------

}
