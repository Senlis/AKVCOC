/*
AKVCOC: web wrapper for church's website and youtube player.
App developed by Richard Romick for use by the Alaska Valley Church of Christ.
Programming help and advice provided by Jacob Harris.
Backend development and UI direction provided by Brady Kuenning.
		Copyright (C) 2018  Richard Romick

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package app_vcoc.akvcoc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity
{
    //==================================== Class variable declarations =========================================
    //view objects that will be used to interact with the UI
    private WebView my_web_view;                            //web browser
    private pl.droidsonroids.gif.GifImageView vcoc_loading; //loading animation
    private BottomNavigationView bottom_nav_view;           //nav bar

    //the various URLs our navigation bar can navigate to
    final private String home_URL = "https://akvcoc.com/app-home";
    final private String events_URL = "https://akvcoc.com/app-events";
    final private String directory_URL = "https://akvcoc.ctrn.co/directory/index.php";
    //---------------------------------- end Class variable declarations ---------------------------------------

    @SuppressLint("SetJavaScriptEnabled") //this keeps Android studio from warning us about enabling javascript
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        /*
        Main Activity constructor
        Sets up web view and navigation bar
        @param Bundle savedInstanceState: saved state from screen rotation
         */

        //==================================== Admin stuff =========================================
        //parent code
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);  //this ties the activity_main.xml file to this class

        //get handles on views (aka: UI objects)
        //reference objects created at the top of the class MainActivity
        bottom_nav_view = findViewById(R.id.navigation);
        my_web_view = findViewById(R.id.web_view); //grab a handle on the web wrapper
        vcoc_loading = findViewById(R.id.vcoc_loading);

        //---------------------------------- end admin stuff ---------------------------------------

        //=================================== Web View setup =======================================
        //of of this code defines the web views configuration at app launch
        my_web_view.loadUrl(home_URL); //load the home page

        my_web_view.getSettings().setJavaScriptEnabled(true); //enable javascript
        /*
            We control all websites the webview goes to, except Facebook
            Therefore, we consider enabling javascript to be safe
         */

        my_web_view.setWebViewClient(new WebViewClient() //this area customizes the web view behavior
        {
            /*
            Custom web view client enables loading animation and overrides behavior of
            phone number and SMS URLs, redirecting them to the phone and SMS app.
            */
            @Override
            public void onPageStarted(WebView webview, String url, Bitmap favicon)
            {
             /*
             Web View listener function
             Activated when web view starts loading a page
             Makes loading animation visible; web view invisible
              */
                vcoc_loading.setVisibility(View.VISIBLE);
                my_web_view.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onPageFinished(WebView view, String url)
            {
                //todo: improve page loading so we don't get flashes of the previous webpage when we are done loading
             /*
             Web View listener function
             Activated when web view finishes loading a page
             Makes loading animation invisible; web view visible
              */
                super.onPageFinished(view, url);

                my_web_view.setVisibility(View.VISIBLE);
                vcoc_loading.setVisibility(View.INVISIBLE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                /*
                Web view listener function
                Activated when user clicks a link
                Redirects URL to phone and sms apps.  Also defines which URLs the WebView can navigate to
                */

                if (url == null) //we must check for null, otherwise our app has the potential to crash
                {
                    show_dark_toast("Error code: 1.  Please report this to the app developers"); //list of error codes on wiki page
                }

                if (url.startsWith("tel:"))
                {
                    //activate device phone application
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    startActivity(intent);
                    view.reload();
                }
                else if (url.startsWith("sms:"))
                {
                    //activate device messaging application
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    view.reload();
                }
                //todo: add map and email integration
                //if user is attempting to navigate to a webpage, either through the nav bar or clicking a link,
                //the following decision statement restricts which webpage the user can go to
                else if (url.contains("akvcoc.com") || url.contains("facebook.com") || url.contains("akvcoc.ctrn.co") || url.contains("youtube.com")) {
                    //if new url matches any of the comparisons above, allow navigating to the page
                    view.loadUrl(url);
                }
                else
                {
                    //if new url doesn't match any of the if statements above, we will display a message that we will not load the page
;                   show_dark_toast("Unable to open link");
                }

                return true; //return true tells the web view to not load the URL on it's own
            }
        });
        //------------------------------- end Web View setup -------------------------------------

        //============================== navigation bar setup =====================================
        //This section defines the behavior of the navigation bar at the bottom of the screen
        bottom_nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener()
        {
            /*
                This listener responds to navigation button presses
             */
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                switch (item.getItemId()) //switches between different button presses
                {
                    case R.id.navigation_home: //home button pressed
                        if (!my_web_view.getUrl().equals(home_URL)) //don't load the home URL if we are already there
                        {
                            //load the new URL
                            my_web_view.loadUrl(home_URL);
                        }
                        return true;
                    case R.id.navigation_events: //events button pressed
                        if (!my_web_view.getUrl().equals(events_URL)) //don't load the events URL if we are already there
                        {
                            //load the new URL
                            my_web_view.loadUrl(events_URL);
                        }
                        return true;
                    case R.id.navigation_directory: //directory button pressed
                        if (!my_web_view.getUrl().equals(directory_URL)) //don't load the directory URL if we are already there
                        {
                            //load the new URL
                            my_web_view.loadUrl(directory_URL);
                        }
                        return true;
                    case R.id.navigation_live: //live button pressed
                        change_to_youtube_activity(); //function changes activity to youtube player

                        return true;
                }
                return false;
            }
        });

        //============================= Transition to Youtube =====================================
        Bundle b = getIntent().getExtras(); //retrieve data passed from previous activity
        int button_num = -1; // variable declaration

        if(b != null) { //a non-null value indicates we are transitioning from youtube activity
	        button_num = b.getInt("button"); //retrieve button that was pressed
            bottom_nav_view.getMenu().getItem(button_num).setChecked(true); //highlight the indicated button

            //below switch statements loads the correct URL based on which button was pressed
            switch (button_num)
            {
                case 0:
                    my_web_view.loadUrl(home_URL);
                    break;
                case 1:
                    my_web_view.loadUrl(events_URL);
                    break;
                case 2:
                    //"URL" references last webpage viewed before youtube acty
                    //or "URL" references the youtube playlist URL for previous services
                    //next line loads that URL
                    my_web_view.loadUrl(b.getString("URL"));
                    highlight_button_by_URL(); //function ensures correct nav button is highlighted
                    break;
                case 3:
                    my_web_view.loadUrl(directory_URL);
            }
        }
        // ----------------------------- end transition to youtube  ------------------------------
    }

    private void highlight_button_by_URL()
    {
        /*
        Highlight Button by URL function
        Used to ensure the correct nav button is highlighted
        Best used when app navigates to a page the user didn't click on
         */

        switch (my_web_view.getUrl()) //retrieve current URL and compare to button's URL
        {
            case events_URL:
                bottom_nav_view.getMenu().getItem(1).setChecked(true); //events button highlighted
                break;
            case directory_URL:
                bottom_nav_view.getMenu().getItem(3).setChecked(true); //directory button highlighted
                break;
            default: //case that URL didn't fit into any previous cases -- or -- home button
                bottom_nav_view.getMenu().getItem(0).setChecked(true); //home button highlighted
        }
    }

    //---------------------------- end navigation bar setup ----------------------------------

    //====================================== misc navigation setup ===============================
	private void change_to_youtube_activity()
	{
		/*
		Change to Youtube Activity function
		This function is used to load the youtube activity
		Function also sends current URL in case user presses back button
		 */
		Intent intent = new Intent(MainActivity.this, YoutubePlayer.class);
		Bundle b = new Bundle();
		b.putString("URL", my_web_view.getUrl()); //Current URL is stored
		intent.putExtras(b); //place URL data in intent
		startActivity(intent); //starts the youtube activity
		this.overridePendingTransition(0, 0); //this cancels view switch animation
		finish(); //this closes the main activity
	}

    public void onBackPressed()
    {
        /*
        Built in On Back Pressed function
        Changes Android back button behavior to act more like a browser
         */
        if (my_web_view.canGoBack()) //if the web view has a previous page to navigate to
        {
            my_web_view.goBack(); //go back to the previous webpage
        }
        else //otherwise
        {
            super.onBackPressed(); //use default app back button behavior
        }
    }
    //--------------------------- end misc navigation setup --------------------------------------

    //============================= screen rotation logic ========================================
    //This section preserves the state of the app when the screen is rotated
    //Android essentially destroys the app and rebuilds it when the screen is rotated
    //If we don't have some sort of logic to preserve state, our app would restart
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)  //@NonNull indicates to Android Studio that outState should never be null
    {
        //function is executed whenever the screen rotates, but before the screen is destroyed
        //we save the current URL so we can load it after the screen is re-created
        super.onSaveInstanceState(outState); //continue with screen destruction
        outState.putString("URL", my_web_view.getUrl()); //save the current URL to the saved state

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState)
    {
        //function is executed whenever the screen is rotated, but after the screen is recreated
        //we have saved the current URL, so we can re-load it
        super.onRestoreInstanceState(savedInstanceState); //first, finish recreating the screen

        String current_url = savedInstanceState.getString("URL");

        //have the web view load the url
        my_web_view.loadUrl(current_url); //load the current URL into the web wrapper
    }
    //--------------------------- end screen rotation logic --------------------------------------

    private void show_dark_toast(String toast_text)
    {
        /*
        Function consolidates logic for creating Toasts (pop-ups)
        @param toast_text - the text the toast should display
        */

        //create a new toast and get a handle on the view
        Toast my_toast = Toast.makeText(getApplicationContext(), toast_text, Toast.LENGTH_LONG);  //create a new toast
        View my_view = my_toast.getView();

        //set toast options
        my_toast.setGravity(Gravity.BOTTOM, 0, 300); //offset the toast further from the bottom to have spacing above nav bar
        my_view.getBackground().setColorFilter(getResources().getColor(R.color.gray), PorterDuff.Mode.SRC_IN); //change toast background color
        TextView my_text = my_toast.getView().findViewById(android.R.id.message); //grab handle on toast text
        my_text.setTextColor(getResources().getColor(R.color.white)); //change text color

        //display toast
        my_toast.show();
    }
}