package app_vcoc.akvcoc;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class Launcher extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);


        //change to MainActivity
        Intent intent = new Intent(Launcher.this, MainActivity.class);
        Bundle b = new Bundle();
        intent.putExtras(b); //place URL data in intent
        startActivity(intent);
        this.overridePendingTransition(0, 0); //this cancels intent animation
        finish();
    }
}
