/*
AKVCOC: web wrapper for church's website and youtube player.
App developed by Richard Romick for use by the Alaska Valley Church of Christ.
Programming help and advice provided by Jacob Harris.
Backend development and UI direction provided by Brady Kuenning.
		Copyright (C) 2018  Richard Romick

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package alaskavalleychurchofchrist.akvcoc;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeInitializationResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;


public class youtube_player extends YouTubeBaseActivity {

    private BottomNavigationView bottom_nav_view;
    private YouTubePlayerView my_youtube_view;
    private YouTubePlayer.OnInitializedListener my_youtube_listener;
    private YouTubePlayer.PlaybackEventListener playback_listener;
    private String API_key;
    private YouTubePlayer player;
    private ImageView pause_play_button;
    private Switch live_switch;
    private String URL;
    private String video_ID;
	//final String VCOC_channel_ID = "UCSJ4gkVC6NrvII8umztf0Ow"; //test channel ID
	final String VCOC_channel_ID = "UCgETYmGUhEmeYYYxVgXKmAA";
	private Boolean load_recording = false;
	private Integer seek_to;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            /*
            Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
            Bundle b = new Bundle();
            b.putInt("key", 1); //Your id
            intent.putExtras(b); //Put your id to your next Intent
            startActivity(intent);
            finish();
            */
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    change_to_main_activity(0);

                    return true;
                case R.id.navigation_events:
                    change_to_main_activity(1);

                    return true;
                case R.id.navigation_directory:
                    change_to_main_activity(3);

                    return true;
                case R.id.navigation_live:

                    return true;
            }
            return false;
        }
    };

    private void change_to_main_activity(int button)
    {
        Intent intent = new Intent(youtube_player.this, MainActivity.class);
        Bundle b = new Bundle();
        b.putInt("button", button); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
	    this.overridePendingTransition(0, 0);
        finish();
    }

	private void change_to_main_activity(String u)
	{
		Intent intent = new Intent(youtube_player.this, MainActivity.class);
		Bundle b = new Bundle();
		b.putInt("button", 2);  //this is a dummy button number to indicate one wasn't pressed
		b.putString("URL", u); //Your id
		intent.putExtras(b); //Put your id to your next Intent
		startActivity(intent);
		this.overridePendingTransition(0, 0);
		finish();
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        video_ID = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_player);

        pause_play_button = (ImageView) findViewById(R.id.pause_play_button);
        live_switch = (Switch) findViewById(R.id.live_switch);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        bottom_nav_view = (BottomNavigationView)findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottom_nav_view);

        bottom_nav_view.getMenu().getItem(2).setChecked(true);

        my_youtube_view = (YouTubePlayerView)findViewById(R.id.youtube_player);

	    //vvvv Start read Youtube API key from protected file vvvv
	    try
	    {
		    InputStream is = getAssets().open("youtubeAPIkey.txt");
		    int size = is.available();
		    byte[] buffer = new byte[size];
		    is.read(buffer);
		    is.close();
		    API_key = new String(buffer);
	    }
	    catch (IOException e)
	    {
	    	e.printStackTrace();
	    }

	    //^^^^ End read Youtube API key from protected file ^^^^



		getLiveIDFromChannel(VCOC_channel_ID, API_key);

        my_youtube_listener = new YouTubePlayer.OnInitializedListener()
        {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b)
            {

	            if (video_ID != null && !load_recording)
	            {
		            Log.i("Youtube", "loading " + video_ID);
		            youTubePlayer.loadVideo(video_ID);
	            } else
	            {
		            Log.i("Youtube", "video ID is null");

		            live_switch.setChecked(true);

		            youTubePlayer.loadPlaylist("UUgETYmGUhEmeYYYxVgXKmAA");

		            if (!load_recording)
		            {

			            Toast.makeText(youtube_player.this, "Live stream unavailable", Toast.LENGTH_SHORT).show();
		            }
	            }


	            youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
                youTubePlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);
                player = youTubePlayer;
                player.setPlaybackEventListener(playback_listener);

                if (load_recording)
                {
                	player.seekToMillis(seek_to);
                }
            }


            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult)
            {
            }
        };



        playback_listener = new YouTubePlayer.PlaybackEventListener()
        {
            @Override
            public void onPaused()
            {
                pause_play_button.setImageResource(R.mipmap.play);
            }

            @Override
            public void onPlaying()
            {
                pause_play_button.setImageResource(R.mipmap.pause);
            }

            @Override
            public void onBuffering(boolean isBuffering)
            {
                return;
            }

            @Override
            public void onStopped()
            {
                return;
            }

            @Override
            public void onSeekTo(int newPositionMillis)
            {
                return;
            }
        };

        my_youtube_view.initialize(API_key, my_youtube_listener);

        //Retrieve URL app was previously on in case back button is pressed
        Bundle b = getIntent().getExtras();
        URL = "";

        if(b != null)
        {
        	URL = b.getString("URL");
        }

    }

    public void getLiveIDFromChannel(String channel_ID, String key)
    {
	    final String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=" + channel_ID + "&eventType=live&type=video&key=" + key;

	    final RequestQueue queue = Volley.newRequestQueue(this);

	    Response.Listener<JSONObject> JSON_listener = new Response.Listener<JSONObject>()
	    {
		    @Override
		    public void onResponse(JSONObject response)
		    {

			    try
			    {
				    JSONObject page_data = response.getJSONObject("pageInfo");
				    Integer num_results = page_data.getInt("totalResults");
				    if (num_results > 0)
				    {
					    // here, we have a live video, so we drill down and find the ID
					    JSONArray video_array = response.getJSONArray("items");
					    JSONObject video_data = video_array.getJSONObject(0);
					    JSONObject video_ID_data = video_data.getJSONObject("id");
					    video_ID = video_ID_data.getString("videoId");
					    Log.i("Parsing", "Video ID=" + video_ID);
				    }
				    else
				    {
					    //todo: switch to recording
					    Log.i("Parsing", "0 results found");
					    video_ID = null;
				    }
			    }
			    catch (JSONException e)
			    {
				    Log.i("JSONException","There was an error:" + e.getMessage());
			    }

		    }

	    };

	    JsonObjectRequest json_data = new JsonObjectRequest(Request.Method.GET, url, null, JSON_listener,
			    new Response.ErrorListener()
			    {
				    @Override
				    public void onErrorResponse(VolleyError error)
				    {
					    Log.i("Error", "onErrorResponse");
				    }
			    }
	    );
	    queue.add(json_data);
    }

    public void play_pause_on_click(View view)
    {
        if (player.isPlaying()) {
            player.pause();
            pause_play_button.setImageResource(R.mipmap.play);
        }
        else {
            player.play();
            pause_play_button.setImageResource(R.mipmap.pause);
        }
    }

    public void switch_moved(View view)
    {
        if (live_switch.isChecked())
        {
            player.loadPlaylist("UUgETYmGUhEmeYYYxVgXKmAA");
            player.play();
            pause_play_button.setImageResource(R.mipmap.pause);
        }
        else {
        	getLiveIDFromChannel(this.VCOC_channel_ID, this.API_key);

        	if (video_ID == null)
	        {
		        live_switch.setChecked(true);
		        Toast.makeText(youtube_player.this, "Live stream unavailable", Toast.LENGTH_SHORT).show();
	        }
        	else
	        {
		        player.loadVideo(this.video_ID);
		        player.play();
		        pause_play_button.setImageResource(R.mipmap.pause);
	        }
        }
    }

    public void onBackPressed()
    {
        //Set up the back button to go back to web view
		change_to_main_activity(this.URL);
    }

	//my code
	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState)
	{
		//function is executed whenever the screen rotates, but before the screen is destroyed
		//we save the current URL so we can load it after the screen is re-created

		//savedInstanceState.putString("URL", my_web_view.getUrl()); //save the current URL to the saved state
		savedInstanceState.putBoolean("Recording", live_switch.isChecked());
		savedInstanceState.putInt("seek_to", player.getCurrentTimeMillis());
		super.onSaveInstanceState(savedInstanceState); //continue with screen destruction
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		//function is executed whenever the screen is rotated, but after the screen is recreated
		//we have saved the current URL, so we can re-load it
		super.onRestoreInstanceState(savedInstanceState); //first, finish recreating the screen

		//String current_url = savedInstanceState.getString("URL");

		//have the web view load the url
		//my_web_view.loadUrl(current_url); //load the current URL into the web wrapper
		load_recording = savedInstanceState.getBoolean("Recording");
		if (load_recording)
		{
			seek_to = savedInstanceState.getInt("seek_to");
		}
	}

}
