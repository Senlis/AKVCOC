/*
AKVCOC: web wrapper for church's website and youtube player.
App developed by Richard Romick for use by the Alaska Valley Church of Christ.
Programming help and advice provided by Jacob Harris.
Backend development and UI direction provided by Brady Kuenning.
		Copyright (C) 2018  Richard Romick

		This program is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
		(at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package alaskavalleychurchofchrist.akvcoc;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;


import android.webkit.WebView;
import android.webkit.WebViewClient;

import android.graphics.Bitmap;
import android.view.View;


public class MainActivity extends AppCompatActivity
{

    private WebView my_web_view;
    private pl.droidsonroids.gif.GifImageView vcoc_loading;
    private BottomNavigationView bottom_nav_view;

    //the various URLs our web wrapper can visit
    private String home_URL;
    private String events_URL;
    private String directory_URL;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottom_nav_view = (BottomNavigationView) findViewById(R.id.navigation);
        bottom_nav_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        my_web_view = (WebView)findViewById(R.id.web_view); //grab a handle on the web wrapper

        vcoc_loading = (pl.droidsonroids.gif.GifImageView)findViewById(R.id.vcoc_loading);

        //set all of our variables for use later
        home_URL = "https://akvcoc.com/app-home";
        events_URL = "https://akvcoc.com/app-events";
        directory_URL = "https://akvcoc.ctrn.co/directory/index.php";

        my_web_view.loadUrl(home_URL);

        my_web_view.getSettings().setJavaScriptEnabled(true);              //and set javascript to true
        my_web_view.setWebViewClient(new CustomWebViewClient());            //set web view client to custom loading page code below

        BottomNavigationViewHelper.disableShiftMode(bottom_nav_view);

        //code to highlight correct button if coming from youtube player
        Bundle b = getIntent().getExtras();
        int button_num = -1; // or other value

        if(b != null) {
	        button_num = b.getInt("button");
            bottom_nav_view.getMenu().getItem(button_num).setChecked(true);

            if (button_num == 0)
            {
	            my_web_view.loadUrl(home_URL);
            }
            else if (button_num == 1)
            {
	            my_web_view.loadUrl(events_URL);
            }
            else if (button_num == 3)
            {
	            my_web_view.loadUrl(directory_URL);
            }
            else if (button_num == 2) //back button was pressed
            {
            	Log.i("Loading", "URL:" + b.getString("URL"));
            	my_web_view.loadUrl(b.getString("URL"));
            	highlight_button_by_URL();
            }
            else
            {
            	Log.i("Error","Activity transition to MainActivity error, value does not represent an existing button");
            }
        }

    }

    private void highlight_button_by_URL()
    {

    	if (my_web_view.getUrl().equals(home_URL))
	    {
		    bottom_nav_view.getMenu().getItem(0).setChecked(true);
	    }
	    else if (my_web_view.getUrl().equals(events_URL))
	    {
		    bottom_nav_view.getMenu().getItem(1).setChecked(true);
	    }
	    else if (my_web_view.getUrl().equals(directory_URL))
	    {
		    bottom_nav_view.getMenu().getItem(3).setChecked(true);
	    }
	    else
	    {
		    bottom_nav_view.getMenu().getItem(0).setChecked(true);
	    }
    }

    private class CustomWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView webview, String url, Bitmap favicon) {
            my_web_view.setVisibility(View.INVISIBLE);
            vcoc_loading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            vcoc_loading.setVisibility(View.INVISIBLE);
            my_web_view.setVisibility(View.VISIBLE);

            super.onPageFinished(view, url);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener()
    {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            switch (item.getItemId())
            {
                case R.id.navigation_home:
                	if (!my_web_view.getUrl().equals(home_URL)) //don't load the home URL if we are already there
	                {
		                my_web_view.loadUrl(home_URL);
	                }
                    return true;
                case R.id.navigation_events:
                	if (!my_web_view.getUrl().equals(events_URL)) //don't load the home URL if we are already there
	                {
		                my_web_view.loadUrl(events_URL);
	                }
                    return true;
                case R.id.navigation_directory:
	                if (!my_web_view.getUrl().equals(directory_URL)) //don't load the home URL if we are already there
	                {
		                my_web_view.loadUrl(directory_URL);
	                }
                    return true;
                case R.id.navigation_live:
                    change_to_youtube_activity();

                    return true;
            }
            return false;
        }
    };

	private void change_to_youtube_activity()
	{
		/*
		This function changes to youtube activity and sends current URL in case user clicks back
		 */
		Intent intent = new Intent(MainActivity.this, youtube_player.class);
		Bundle b = new Bundle();
		b.putString("URL", my_web_view.getUrl()); //Your id
		intent.putExtras(b); //Put your id to your next Intent
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(intent);
		this.overridePendingTransition(0, 0);
		finish();
	}

    public void onBackPressed()
    {
        //Set up the back button to work more like a browser back button
        if (my_web_view.canGoBack())
        {
            my_web_view.goBack();
        }
        else
        {
            super.onBackPressed();
        }
    }


    //my code
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState)
    {
        //function is executed whenever the screen rotates, but before the screen is destroyed
        //we save the current URL so we can load it after the screen is re-created

        savedInstanceState.putString("URL", my_web_view.getUrl()); //save the current URL to the saved state
        super.onSaveInstanceState(savedInstanceState); //continue with screen destruction
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        //function is executed whenever the screen is rotated, but after the screen is recreated
        //we have saved the current URL, so we can re-load it
        super.onRestoreInstanceState(savedInstanceState); //first, finish recreating the screen

        String current_url = savedInstanceState.getString("URL");

        //have the web view load the url
        my_web_view.loadUrl(current_url); //load the current URL into the web wrapper
    }

}

