/*
 AKVCOC: web wrapper for church's website and youtube player.
 App developed by Richard Romick for use by the Alaska Valley Church of Christ.
 Programming help and advice provided by Jacob Harris.
 Backend development and UI direction provided by Brady Kuenning.
 Copyright (C) 2018  Richard Romick
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import UIKit // standard app UI functionality
import WebKit // allows our use of the WKWebView (web wrapper)
import FLAnimatedImage // enables our loading GIF

class ViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, parent_webview //parent_webview is used to pass URLs back from youtube view upon youtube view dismissal
{
    // MARK: - Class properties
    //**************************************************************************************
    let home_URL = URL(string:"https://akvcoc.com/app-home")
    let events_URL = URL(string:"https://akvcoc.com/app-events")
    let directory_URL = URL(string: "https://akvcoc.ctrn.co/directory/index.php")
    
    var last_view_YT = false // variable is true when we just transitioned from the youtube view
    // this affects back button behavior, as the web wrapper does not need to "back", but rather
    // the programe needs to load the youtube view

    // ----------------------------- end Class properties ----------------------------------

    // MARK: - UI element handles
    //**************************************************************************************
    @IBOutlet var web_view: WKWebView! // our browser
    @IBOutlet weak var web_view_container: UIView! // view that contains the web view
    
    @IBOutlet weak var home_button: UIButton!
    @IBOutlet weak var events_button: UIButton!
    @IBOutlet weak var live_button: UIButton!
    @IBOutlet weak var directory_button: UIButton!
    @IBOutlet weak var back_button: UIButton!
    
    @IBOutlet weak var loading_GIF: FLAnimatedImageView!
    
    // ------------------------------- end UI element handles -------------------------------

    // MARK: - Main View and Web view setup Setup
    //***************************************************************************************
    override func viewDidLoad()
    {
        super.viewDidLoad() //parent view class code.  I don't know what it does, but seems important
        
        //set status bar color to black
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return } //get handle on statusBar
        statusBar.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1) //then set status bar color to black
        
        //web view setup
        web_view = WKWebView(frame: web_view_container.bounds, configuration: WKWebViewConfiguration()) //get handle on web view
        web_view.autoresizingMask = [.flexibleWidth, .flexibleHeight] //set the web view size as flexible (actual size handled via storyboard settings)
        web_view.navigationDelegate = self //allows view controller to control web view navigation.  See override functions below
        //the following two function calls ensures that the web_view and loading GIF always occupies the same space
        self.web_view_container.addSubview(web_view) 
        self.web_view_container.addSubview(loading_GIF)
        
        //load animation from file
        if let path = Bundle.main.path(forResource: "vcocloading", ofType: "gif") //this is an optional binding, only uses path if not nil
        {
            //this code only executes if the gif resource is found
            let url = URL(fileURLWithPath: path) //turn file path into a URL
            let gif_data = try? Data(contentsOf: url) //try to load contents of the URL
            let image_data = FLAnimatedImage(animatedGIFData: gif_data) //try to turn contents into a FLAnimatedImage
            loading_GIF.animatedImage = image_data //and turn the FLAnimatedImage into a GIF
        }
        else
        {
            //executes if we weren't able to find gif resource
            print("Unable to locate GIF file")
        }
        
        //load first page
        load_new_URL(requested_URL: home_URL!)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        /*
         Function overrides default status bar text style
         It makes the status bar text white
         The status bar background is set to black in viewDidLoad() -- look at function above
        */
        return .lightContent
    }
    // ------------------------ end main view and web view setups -------------------------------

    // MARK: - Button logic and navigation
    //**************************************************************************************
    @IBAction func navigation_clicked(_ sender: UIButton)
    {
        /*
        Function reacts to all button presses on navigation bar except for "Live" and "Back"
        Back button has it's own function below
        Live is hard wired via Storyboard to load Youtube view (see transition to youtube section below)
        @param sender (UIButton) - the button that called this function
        */
        
        //examine which button was pressed and load that URL into requested URL
        //if statements are set to not execute if the user presses the nav button
        //for the current page (on events page, and the user presses the events button)
        var requested_URL : URL?
        if sender.tag == 1 && web_view.url != home_URL //Home was pressed
        {
            requested_URL = home_URL
        }
        else if sender.tag == 2 && web_view.url != events_URL //Events was pressed
        {
            requested_URL = events_URL
        }
        else if sender.tag == 4 && web_view.url != directory_URL //Directory was pressed
        {
            requested_URL = directory_URL
        }
        else //It shouldn't be possible for the previous three decisions to be false
        {
                return //but if it is, do nothing
        }
        
        //next, we call the function to load the requested page
        if let URL_request = requested_URL //run next lines of code only if we have a non-nil requested_URL
        {
            load_new_URL(requested_URL: URL_request) //load the URL
        }
        else //requested_URL is nil, so something has gone horribly wrong
        {
            print("An error has occured: ViewController>navigation_clicked requested_URL value is nil")
        }
    }
    
    func load_new_URL(requested_URL : URL)
    {
        /*
        Function reacts to all button presses on navigation bar except for "Live" and "Back"
        Back button has it's own function below
        Live is hard wired via Storyboard to load Youtube view (see transition to youtube section below)
        @param sender (UIButton) - the button that called this function
        */
        let my_request = URLRequest(url: requested_URL) //prepare the URL for the web view to load
        web_view.load(my_request) //load the URL that was requested
        set_button_alpha() //make sure the correct button is darkened
        
        last_view_YT = false //indicates to back button event not to load youtube view if button is pressed
        //this will always be the case unless we just transitioned from youtube view.  If that is the case
        //the prepare_for_YT_transition will set last_view_YT to true manually
    }

    func set_button_alpha()
    {
        /*
            Function detects which button is active by current URL and darkens the correct one
        */
        reset_button_alpha() //start by graying all buttons
        if web_view.url == events_URL
        {
            events_button.alpha = 1 //effectively turns button image black
        }
        else if web_view.url == directory_URL 
        {
            directory_button.alpha = 1
        }
        else
        {
            home_button.alpha = 1
        }
    }
    
    func reset_button_alpha()
    {
        /*
         Function grays all buttons
         */
        home_button.alpha = 0.5
        events_button.alpha = 0.5
        directory_button.alpha = 0.5
    }
    
    @IBAction func back_button_pressed(_ sender: UIButton)
    {
        /*
        Function instructs the web view to go back a page unless the last "page" was
        the youtube view, in which case the function would segue into Youtubeview
        */
        if last_view_YT //in this case, back button should load youtube view as it was last page
        {
            performSegue(withIdentifier: "transition_to_youtube", sender: self) //switch to Youtube View
        }
        else if web_view.canGoBack //go back if possible
        {
            web_view.goBack()  //web view goes back a page
            set_button_alpha() //ensure correct button is darkened
        }
    }
    // -------------------- end button logic and navigation --------------------------------
    
    // MARK: - Web View Navigation overrides
    //**************************************************************************************
    func webView(_ webView: WKWebView, didStartProvisionalNavigation: WKNavigation!)
    {
        /*
            When the web view starts loading a page
        */
        loading_GIF.isHidden = false //makes the loading gif visible
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        /*
            When the web view is finished loading
        */
        loading_GIF.isHidden = true //makes the gif invisible
        
        //set back button color
        if web_view.canGoBack || last_view_YT //either the web view can go back or we can go to youtube view
        {
            back_button.alpha = 1 //black
        }
        else
        {
            back_button.alpha = 0.5 //gray
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    {
        /*
            When a user clicks a link
            This overide is designed to handle SMS and phone links, as well as whitelisting where the web view can go
        */
        if let URL_as_string = navigationAction.request.url?.absoluteString
        {
            //override tel and sms links to phone app
            if let request_URL = navigationAction.request.url, request_URL.scheme == "tel" || request_URL.scheme == "sms"
            {   //executes only if request_URL isn't nil and is of type tel or sms
                UIApplication.shared.open(request_URL, options: [:], completionHandler: nil) //open url in default application
                decisionHandler(.cancel) //stop this apps handling of the URL
            }
            //if not tel or sms, decide if the whitelist allows the link
            else if URL_as_string.contains("akvcoc.com") || URL_as_string.contains("https://www.facebook.com") || URL_as_string.contains("https://m.facebook.com") || URL_as_string.contains("youtube.com") || URL_as_string.contains("akvcoc.ctrn.co") || URL_as_string.contains("about:blank") //whitelisted URLs
            {
                decisionHandler(.allow) //allow this app to continue processing URL
            }
            //if not either of the above, navigation is not allowed
            else
            {
                //create an alert and display it
                let my_alert = UIAlertController(title: "Navigation not possible", message: "Unable to open link", preferredStyle: .alert)
                my_alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in NSLog("Navigation not permitted alert occured.")}))
                decisionHandler(.cancel) //URL was not whitelisted
                self.present(my_alert, animated: true, completion: nil) //present the allert
            }
        }
        else  //in this case, the requested URL was somehow empty
        {
            decisionHandler(.cancel)  //do nothing
        }
    }
    //--------------------- end web view navigation overrides ------------------------------

    // MARK: - Youtube transitions
    //**************************************************************************************    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        /*
         Function is called before switching to another view controller (in this case, Youtube controller)
        */
        //grab handle on next view controller
        let destination_VC = segue.destination as! YoutubeViewController
        
        //send the URL to be stored for the back button
        destination_VC.previous_URL = web_view.url
        
        destination_VC.delegate = self //set ourselves as the delegate of the youtube view so it can control it
    }
    
        
    func prepare_for_YT_transition(data: URL)
    {
        /*
            function is used by youtube view to pass URLs to web view to load before transitioning back to main viewcontroller
            this function is from the protocol parent_webview
            this function will always be run when returning from youtube view
            @param data(URL) - represents the button that was pressed
        */
        
        if data != URL(string: "backbutton") //if an "backbutton" URL is passed, web view is alread on the page that needs to load
        {
            load_new_URL(requested_URL: data) //changes Webview URL and colors correct button black
        }
        last_view_YT = true //indicates to back button event to load youtube view if button is pressed
    }
    //----------------------------------- end Youtube transitions ------------------------------------
}
