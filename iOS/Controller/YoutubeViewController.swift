/*
 AKVCOC: web wrapper for church's website and youtube player.
 App developed by Richard Romick for use by the Alaska Valley Church of Christ.
 Programming help and advice provided by Jacob Harris.
 Backend development and UI direction provided by Brady Kuenning.
 Copyright (C) 2018  Richard Romick
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import UIKit
import WebKit
import youtube_ios_player_helper
import SwiftyJSON
import Alamofire

protocol parent_webview
{
    /*
     This is a handle on a function in the main activity (which is still active)
     Prepare_for_YT_transition is a way for us to pass URLs to the main activity for it to load
     Typically, it is used before dismissing this activity
    */
    func prepare_for_YT_transition(data: URL)
}

class YoutubeViewController: UIViewController
{
    // MARK: - Class properties
    //**************************************************************************************
    let home_URL = URL(string:"https://akvcoc.com/app-home")
    let events_URL = URL(string:"https://akvcoc.com/app-events")
    let directory_URL = URL(string:"https://akvcoc.ctrn.co/directory/index.php")
    let recorded_services_URL = URL(string:"https://www.youtube.com/user/AKVCOC/videos")
    var previous_URL : URL? //used to store previous URL for back button
    var loading_URL : URL? //during seque, this will store which URL to transition to

    @IBOutlet weak var youtube_view: YTPlayerView!  //handle on the youtube play UI element
    let test_video_ID : String = "wZZ7oFKsKzY"      //test video
    let test_channel_ID : String = "UCLA_DiR1FfKNvjuUpBHmylQ"  //this channel ID should always be playing (but double check before using)
    let VCOC_channel_ID : String = "UCgETYmGUhEmeYYYxVgXKmAA"  //church channel
    var youtube_API_key : String? //this will be loaded later from file
    let url_first_half : String = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId="  //first half of an API call
    let url_second_half : String = "&eventType=live&type=video&key="  //second half of an API call
    var JSON_video_ID : String?  //what we want to retrieve from the JSON request
    var JSON_url : String? //All URLs put together for the JSON request
    
    @IBOutlet weak var not_available_label: UILabel!  //UI element used if the church video is not streaming
    @IBOutlet weak var back_button: UIButton!  //UI element for back button (we need to change its color when available
    
    var delegate : parent_webview? //handle on delegate class. May be nil
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        /*
         This changes the text on the status bar to white
        */
        
        return .lightContent
    }
    
    //------------------------------------ end class properties -------------------------
    
    // Mark: - View setup
    //*************************************************************************************
    override func viewDidLoad()
    {
        super.viewDidLoad() //call the parent function.  Don't know what they do up there
        
        youtube_view.setPlaybackQuality(YTPlaybackQuality.auto) //let the youtube player decide video quality
        
        if previous_URL == nil //if we don't have a URL to go back to
        {
            back_button.alpha = 0.5 //set the back button to gray
            //todo: is this condition really possible?  We are transitioning from main activity, which should have had passed some URL
        }
        // MARK: - Read from Youtube Key file
        //*********************************************************************************************
        if let dir = Bundle.main.path(forResource: "YoutubeAPIKey", ofType: "txt")  //line searches for YoutubeAPIKey.txt in bundle resources
        {

            do // if we find it
            {
                youtube_API_key = try String(contentsOfFile: dir).trimmingCharacters(in: .newlines)  //reads contents of YoutubeAPIKey.txt and removes newlines
            }
            catch // if we don't find it
            {
                //todo: perhaps this should be a toast
                print("YoutubeViewController.viewDidLoad() Youtube API key File data not found")
            }
        }
        else
        {
            //todo: perhaps this should be a toast
            print("YoutubeViewController.viewDidLoad() Youtube API key Directory file not found")
        }
        
        if let youtube_key = youtube_API_key
        {
            JSON_url = url_first_half + VCOC_channel_ID + url_second_half + youtube_key  //constructs URL for JSON request
//            JSON_url = url_first_half + test_channel_ID + url_second_half + youtube_key  //uncomment to use test live video (comment line above)
        }
        //------------------------------ Read from Youtube Key file ------------------------------------
        
        // MARK: - JSON Request
        //*********************************************************************************************
        if let my_url = JSON_url
        {
            Alamofire.request(my_url, method: .get).responseJSON //alamo fire is our tool for conducting the JSON request
            {
                response in  //results are stored in the variable response
                if response.result.isSuccess
                {
                    let JSON_data : JSON = JSON(response.result.value!) //convert response to JSON object
                    
                    if JSON_data["pageInfo"]["totalResults"] > 0 //this means we got a live video
                    {
                        self.JSON_video_ID = "\(JSON_data["items"][0]["id"]["videoId"])" //store the video ID
                        
                        if let video_ID = self.JSON_video_ID
                        {
                            self.youtube_view.load(withVideoId: video_ID)
                        }
                        else //error state: video was found, but there was no data in the results
                        {
                            // todo: better error handling.  Perhaps start JSON request again
                            print("Results found, but no video ID found")
                        }
                    }
                    else //case that JSON request was successful, but no live youtube video
                    {
                        self.youtube_view.isHidden = true //make the youtube object hidden
                        self.not_available_label.isHidden = false //show the video not available label
                        print("no live youtube video")
                    }
                }
                else //error state: JSON request did not go through
                {
                    //todo: this should be a toast.  Perhaps no internet connection logic should be implemented
                    print("JSON response failure")
                }
            }
        }
        // --------------------------------- end JSON request ----------------------------
    }
    
    // MARK: - Navigation
    //**********************************************************
    @IBAction func button_clicked(_ sender: UIButton)
    {
        switch sender.tag
        {
        case 1: //home button pressed
            loading_URL = home_URL
            break
        case 2: //events button pressed
            loading_URL = events_URL
            break
        case 4: //directory button pressed
            loading_URL = directory_URL
            break
        case 5: //Recorded Services button pressed
            loading_URL = recorded_services_URL
        case 6: //Back button pressed
            //if back button is pressed, we go straight to previous view with no change
            loading_URL = URL(string: "backbutton") //we indicate to not load anything with the URL "backbutton"
            break
        default: //live button pressed...do nothing
            return
        }
        
        //transition to web view.  When we call this command, our loading_URL will be sent
        if let url_to_load = loading_URL //execute only if loading_URL is not nil.  It should never be nil
        {
            delegate?.prepare_for_YT_transition(data: url_to_load) //we send the URL to the parent webview
            dismiss(animated: false, completion: nil) //transition to web_view with no animation and nothing additional needs to be done
        }
    }
    //------------------------- Navigation -------------------------------
}

